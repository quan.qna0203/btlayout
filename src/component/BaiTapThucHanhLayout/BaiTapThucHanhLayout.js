import React from 'react'
import HeaderBTLayout from './HeaderBTLayout'
import BodyBTLayout from './Body/BodyBTLayout'
import FooterBTLayout from './FooterBTLayout'

export default function BaiTapThucHanhLayout() {
    return (
        <div>
            <div className="header">
                <HeaderBTLayout />
            </div>
            <BodyBTLayout />
            <footer className="py-5 bg-dark">
                <FooterBTLayout />
            </footer>
        </div>
    )
}
