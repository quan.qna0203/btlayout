import React from 'react'

export default function FooterBTLayout() {
    return (
        <div className="container"><p className="m-0 text-center text-white">Copyright © Your Website 2023</p></div>
    )
}
