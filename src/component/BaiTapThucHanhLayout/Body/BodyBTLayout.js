import React from 'react'
import BannerBTLayout from './BannerBTLayout'
import ItemListBTLayout from './ItemListBTLayout'

export default function BodyBTLayout() {
  return (
    <div className='body container py-5'>
        <BannerBTLayout/>
        <ItemListBTLayout/>
    </div>
    
  )
}
