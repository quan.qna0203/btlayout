import React from 'react'
import ItemBTLayout from './ItemBTLayout'

export default function ItemListBTLayout() {
    return (
        <div className='pt-4 '>
            <div className="container px-lg-5">
                <div className="row">
                   <ItemBTLayout/>
                   <ItemBTLayout/>
                   <ItemBTLayout/>
                   <ItemBTLayout/>
                   <ItemBTLayout/>
                   <ItemBTLayout/>

                </div>
            </div>
        </div>
    )
}
